<?php
/**
 * Created by PhpStorm.
 * User: Maurice
 * Date: 4-11-2017
 * Time: 21:58
 */
require('fpdf/fpdf.php');

$cursusnaam = $_GET['cursus'];
$datum = $_GET['datum'];
$locatie = $_GET['locatie'];

//print_r(array_values($_POST));

$data = $_POST;

// De standaard PHP manier
//
//foreach($data as $i => $item) {
//        echo "<h1>Attest voor de cursus " . $cursusnaam . "</h1>";
//        echo "<p>We feliciteren " . $data[$i] . " met het behalen van de cursus " . $cursusnaam . ".</p>";
//        echo "<p></p>";
//        echo "Behaald in " . $locatie . " op datum : " . $datum . ".";
//        echo "<p></p>";
//        echo "-------";
//    }


// De FPDF manier

$pdf = new FPDF();

foreach($data as $i => $item) {

    $pdf->SetTopMargin(50);
    $pdf->AddPage(); // telkens een nieuwe pagina
    $pdf->Image('syntra.png',10,6,30);
    $pdf->SetFont('Arial','',30);
    $pdf->Cell(190, 8, "Attest voor de cursus " . $cursusnaam ,0,1,'C',0);
    $pdf->Ln();
    $pdf->SetFont('Arial','B',16);
    $pdf->Cell(190, 8, "We feliciteren " . $data[$i] . " met het behalen van de cursus " ,0,1,'C',0);
    $pdf->Ln();
    $pdf->SetFont('Arial','I',16);
    $pdf->Cell(190, 8, "Behaald in " . $locatie . " op datum : " . $datum,0,1,'C');

}
$pdf->Output();

