<?php
/**
 * Created by PhpStorm.
 * User: Maurice
 * Date: 4-11-2017
 * Time: 20:18
 */

$uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/uploads/';
$uploadfile = $uploaddir . basename($_FILES['userfile']['name']);


if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
    echo "Het CSV bestand is met succes geupload.<br><br>";
    echo "<a href='index.php'>Keer terug naar invoerscherm om de cursusinformatie in te voeren.</a>" ;
} else {
    echo "Probleem met uploaden, probeer opnieuw";
}

?>